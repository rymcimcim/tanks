import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;


public class Wall {

	private double x;
	private double y;
	private Animation wall1;
	private Animation wall2;
	private Sprite sprite;
	
	public Wall(double x, double y, int i) {
		this.x = x;
		this.y = y;
		loadWalls();
		
		if (i == 1)
			sprite = new Sprite(wall1, (float)this.getX(), (float)this.getY());
		else if (i == 2)
			sprite = new Sprite(wall2, (float)this.getX(), (float)this.getY());
	}
	
	public void loadWalls() {
		BufferedImage wall1img = loadImage("wall1.png");
		BufferedImage wall2img = loadImage("wall2.png");
		
		wall1 = new Animation();
		wall2 = new Animation();
		wall1.addFrame(wall1img, 1000000000);
		wall2.addFrame(wall2img, 1000000000);
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	
	
	public BufferedImage loadImage(String fnm) {        
		 try {       
			 BufferedImage im = ImageIO.read(getClass().getResource(fnm));
			 return im;
		     
		 }     
		 catch(IOException e) {
			 System.out.println("Load Image error for " + fnm + ":\n" + e);    
			 return null;
		 }  
	 } // end of loadImage()
	
	public void draw(Graphics2D g) {
    	g.drawImage(sprite.getImage(), (int)sprite.getx(), (int)sprite.gety(), null);
	}
}
