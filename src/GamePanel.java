import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.IOException;
import java.text.DecimalFormat;

import javax.swing.JPanel;


public class GamePanel extends JPanel implements Runnable {
	
	private static final int PWIDTH = 435;
	private static final int PHEIGHT = 455;
	
	private long timeDiff;
	private long timeDiffSum;

	private DecimalFormat df;
	
	public int i;
	
	private double fps;
	
	private Thread animator;
	private volatile boolean running = false;
	
	private volatile boolean gameOver = false;
	
	private Graphics2D db2Dg;
	private Image dbImage = null;
	
	private ObjectManager manager;
	private Controller controller;
	
	
	/* more variables,
	 * explained later
	 */
	
	public GamePanel() throws IOException {
		setSize(PWIDTH, PHEIGHT);
		setBackground(Color.GRAY);
		setFocusable(true);
		setDoubleBuffered(true);
		df = new DecimalFormat("0.##");
		manager = new ObjectManager();
		
		controller = new Controller(manager);
		addKeyListener(controller);
		/* create game components
		 * 		...
		 */
	} // end of GamePanel()

	
	public void addNotify() {
		super.addNotify();
		startGame();
	}
	
	
	public void startGame() {
		if (animator == null || !running) {
			animator = new Thread(this);
			animator.start();
		}
	}
	
	public void stopGame()
	{ running = false; }
	
	@Override
	public void run() {
		
		long beforeTime;
		
		
		running = true;
		while(running) {
			
			beforeTime = System.nanoTime();
			
			
			gameUpdate();
			gameRender();
			paintScreen();
			
			try {
				Thread.sleep(5);
			}
			catch(InterruptedException ex) {}
			
			timeDiff = System.nanoTime() - beforeTime;
		}
		System.exit(0);
	} //end of run()

	
	private void gameUpdate() {
		if (!gameOver) {
			// update game state
			countFPS();
			manager.run(timeDiff);
		}
	}
	
	
	private void gameRender() {
		
			dbImage = createImage(PWIDTH, PHEIGHT);
			if (dbImage == null) {
				System.out.println("dbImage is null");
				return;
			}
			else {
				db2Dg = (Graphics2D)dbImage.getGraphics();
			}
			
			db2Dg.setColor(Color.BLACK);
			db2Dg.fillRect(0, 0, PWIDTH, PHEIGHT);
			
			manager.draw(db2Dg);
			
			db2Dg.setColor(Color.RED);
			db2Dg.drawString("FPS: ", 5, 10);
			db2Dg.drawString(df.format(fps), 35, 10);
			
			
			// draw game elements
			// ...
			 
	} // end of gameRender()
	
	private void paintScreen() {
		try {
			Graphics2D g = (Graphics2D)this.getGraphics();
			if ((g != null) && (dbImage != null))
				g.drawImage(dbImage, 0, 0, null);
			Toolkit.getDefaultToolkit().sync();
			g.dispose();
		}
		catch (Exception e) {
			System.out.println("Graphics context error: " + e);
		}
	} // end of paintScreen()
	
	public void countFPS(){
		timeDiffSum += timeDiff;
		
		i++;
		
		if ((float)timeDiffSum / 1000000000F > 1){		// co sekunde oblicza FPSy
			if ( i > 10){
				float srednia = timeDiffSum / i;
				fps = (float)(1000000000F / srednia);
				timeDiffSum = 0;
				i = 0;
			}
		}
	}
} // end of GamePanel class


