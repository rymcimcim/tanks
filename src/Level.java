
public class Level {
	
	public int width;
	public int height;
	
	public int grid[][];
	
	public Level(int w, int h){
		width = w;
		height = h;
		grid = new int[w][h];
	}
	
	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public void setGridValue(int x, int y, int v){
		grid[x][y] = v;
	}
	
	public int getGridValue(int x, int y) {
		return grid[x][y];
	}
}
