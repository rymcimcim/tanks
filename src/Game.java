import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.io.IOException;

import javax.swing.JFrame;

public class Game {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			
			@Override
			public void run(){
				GameFrame frame = new GameFrame();
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				try {
					frame.init();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
	}
}

class GameFrame extends JFrame {
	
	public static final int WIDTH = 435;
	public static final int HEIGHT = 455;
	
	public GameFrame() {
		setSize(new Dimension(WIDTH, HEIGHT));
		setTitle("Giereczka");
	}
	
	public void init() throws IOException{
		GamePanel gp = new GamePanel();
		
		gp.setBounds(0, 0, WIDTH, HEIGHT);
		Container contentPane = this.getContentPane();
		contentPane.add(gp);
		
		gp.grabFocus();
		//gp.run();
	}
	
}