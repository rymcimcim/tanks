import java.awt.Graphics2D;
import java.io.IOException;


public class ObjectManager {
	
	private LevelCreator levelcreator;
	private Level level;
	private MapDrawer md;
	private double mapPosition;
	private Tank tank;

	public ObjectManager() throws IOException {
		levelcreator = new LevelCreator();
		level = levelcreator.createLevel("level.txt");
		mapPosition = 0.0;
		md = new MapDrawer();
		
		tank = new Tank();
	}
	
	public void run(long time) {
		changePlayerPosition(time);
		tank.updateSprite(time);
	}
	
	public void detectPlayerCollision() {
		if (tank.getX() < 0) 
			tank.setX(0);
		if ((tank.getX() + 32) > 416)
			tank.setX(384);
		if (tank.getY() < 0)
			tank.setY(0);
		if ((tank.getY() + 32) > 416)
			tank.setY(384);
		
	}
	
	public void changePlayerDir(int dir){
		if (dir == 0 )
			tank.startMoving(0);
		else if (dir == 1)
			tank.startMoving(1);
		else if (dir == 2)
			tank.startMoving(2);
		else if (dir == 3)
			tank.startMoving(3);
		
	}
	
	public void stopPlayerDir(int dir){
		if (dir == 0 )
			tank.stopMoving(0);
		else if (dir == 1)
			tank.stopMoving(1);
		else if (dir == 2)
			tank.stopMoving(2);
		else if (dir == 3)
			tank.stopMoving(3);
	}
	
	
	
	public void changePlayerPosition(long time){
		if (tank.isMoveUp()){
			double roadY = tank.getVelocity() * (time/1000000000.0);
			tank.setY(tank.getY() - roadY);
			tank.setX(tank.getX());
		}
		
		if (tank.isMoveRight()){
			double roadX = tank.getVelocity() * (time/1000000000.0);
			tank.setX(tank.getX() + roadX);
			tank.setY(tank.getY());
		}
		
		if (tank.isMoveDown()){
			double roadY = tank.getVelocity() * (time/1000000000.0);
			tank.setY(tank.getY() + roadY);
			tank.setX(tank.getX());
		}
		
		if (tank.isMoveLeft()){
			double roadX = tank.getVelocity() * (time/1000000000.0);
			tank.setX(tank.getX() - roadX);
			tank.setY(tank.getY());
		}
		
		tank.updateDir();
		detectPlayerCollision();
	}
	
	public void draw(Graphics2D g) {
		md.drawMap(g, level, mapPosition);
		tank.draw(g);
	}
}
