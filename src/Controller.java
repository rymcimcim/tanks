import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Controller implements KeyListener{
    
    private ObjectManager manager;
	
    public Controller(ObjectManager m){
        manager = m;
    }
    
    public void keyPressed(KeyEvent e) {
        int KeyCode = e.getKeyCode();
        
        if (KeyCode == KeyEvent.VK_UP){
        	manager.changePlayerDir(0);
        }
        if (KeyCode == KeyEvent.VK_RIGHT){
        	manager.changePlayerDir(1);
        }
        if (KeyCode == KeyEvent.VK_DOWN){
        	manager.changePlayerDir(2);
        }
        if (KeyCode == KeyEvent.VK_LEFT){
        	manager.changePlayerDir(3);
        }
        if (KeyCode == KeyEvent.VK_ESCAPE){
        	System.exit(0);
        }

        e.consume();
    }
    
    public void keyReleased(KeyEvent e){
        int KeyCode = e.getKeyCode();
        
        if (KeyCode == KeyEvent.VK_UP){
        	manager.stopPlayerDir(0);
        }
        
        if (KeyCode == KeyEvent.VK_RIGHT){
        	manager.stopPlayerDir(1);
        }
        
        if (KeyCode == KeyEvent.VK_DOWN){
        	manager.stopPlayerDir(2);
        }
        if (KeyCode == KeyEvent.VK_LEFT){
        	manager.stopPlayerDir(3);
        }
         
        e.consume();
    }
    
    public void keyTyped(KeyEvent e){
        e.consume();
    }
}
