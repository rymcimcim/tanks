import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;


public class MapDrawer {
	
	private Image floor;
	private ArrayList<Wall> walls;
	
	public MapDrawer(){
		loadImages();
		walls = new ArrayList<Wall>();
	}
	
	public void loadImages(){
		floor = loadImage("floor.png");
		
	}
	
	public void drawMap(Graphics2D g, Level level, double mapPosition){
		
		for (int i = 0; i < level.getHeight(); i++){
			for (int j = 0; j < level.getWidth(); j++){
				
				if (level.getGridValue(j, i) == 0){
					g.drawImage(floor, j*32, i*32, null);
				}
				else if (level.getGridValue(j, i) == 1){
					Wall wall = new Wall(j*32, i*32, 1);
					walls.add(wall);
				}
				else if (level.getGridValue(j, i) == 2){
					Wall wall = new Wall(j*32, i*32, 2);
					walls.add(wall);
				}
			}
		}
		for (Wall w : walls) {
			w.draw(g);
		}
	}
	
	public BufferedImage loadImage(String fnm) {        
		 try {       
			 BufferedImage im = ImageIO.read(getClass().getResource(fnm));
			 return im;
		     
		 }     
		 catch(IOException e) {
			 System.out.println("Load Image error for " + fnm + ":\n" + e);    
			 return null;
		 }  
	 } // end of loadImage()
}
