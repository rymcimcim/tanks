import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class LevelCreator {
	
	private int width;
	private int height;
	
	private BufferedReader reader;
	private ArrayList<String> lines;
	private int[][] grid;

	
	public LevelCreator(){
		
		lines = new ArrayList<String>();
		
	}
	
	public int[][] loadMap(String filename) throws IOException{
		
		
		
		try {
			reader = new BufferedReader( new FileReader(filename));
		}
		catch (FileNotFoundException e) {};
		
		while (true){
			
			//String line = reader.readLine();
			
			lines.add(reader.readLine());
			//System.out.println(lines.get(i));
			if (lines.get(lines.size()-1) == null)
				break;
		}
		
		width = lines.get(0).length();
		height = lines.size()-1;
		
		grid = new int[width][height];

		
		reader.close();
		
		for (int i = 0; i < height; i++){
			
			for (int j = 0; j < width; j++){
				
				grid[j][i] = Integer.parseInt(lines.get(i).substring(j, j+1));
				
			}

			//System.out.println();
		}
		
		return grid;
	}
	
	public Level createLevel(String filename) throws IOException{
		loadMap(filename);
		
		Level level = new Level(width, height);
		
		for (int i = 0; i < height; i++){
			
			for (int j = 0; j < width; j++){
				level.setGridValue(j, i, grid[j][i]);
			}
		}
		
		return level;
		
	}
}
