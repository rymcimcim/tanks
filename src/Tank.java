import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;


public class Tank {

	private double x;
	private double y;
	private double velocity;
	
	private boolean moveUp;
	private boolean moveRight;
	private boolean moveDown;
	private boolean moveLeft;
	
	private Animation upAnim;
	private Animation rightAnim;
	private Animation downAnim;
	private Animation leftAnim;
	
	private Animation muAnim;
	private Animation mrAnim;
	private Animation mdAnim;
	private Animation mlAnim;
	
	private Animation currentAnim;
	
	private Sprite sprite;
	
	public Tank(){
		
		x = 192;
		y = 423;
		
		velocity = 200;
		loadAnims();
		currentAnim = upAnim;
		
		sprite = new Sprite(currentAnim, (float)this.getX(), (float)this.getY());
		
	}
	
	public void loadUpAnim() {
		BufferedImage tankImage1 = loadImage("tank1.png");
		
		upAnim = new Animation();
		upAnim.addFrame(tankImage1, 250000000);
		
	}
	
	public void loadRightAnim() {
		BufferedImage ptankImage1 = loadImage("ptank1.png");
		
		rightAnim = new Animation();
		rightAnim.addFrame(ptankImage1, 250000000);
		
	}
	
	public void loadDownAnim() {
		BufferedImage dtankImage1 = loadImage("dtank1.png");
		
		downAnim = new Animation();
		downAnim.addFrame(dtankImage1, 250000000);
		
	}
	
	public void loadleftAnim() {
		BufferedImage ltankImage1 = loadImage("ltank1.png");
		
		leftAnim = new Animation();
		leftAnim.addFrame(ltankImage1, 250000000);
		
	}
	
	public void loadMoveUpAnim() {
		BufferedImage tankImage1 = loadImage("tank1.png");
		BufferedImage tankImage2 = loadImage("tank2.png");
		
		muAnim = new Animation();
		muAnim.addFrame(tankImage1, 250000000);
		muAnim.addFrame(tankImage2, 250000000);
	}
	
	public void loadMoveRightAnim() {
		BufferedImage tankImage1 = loadImage("ptank1.png");
		BufferedImage tankImage2 = loadImage("ptank2.png");
		
		mrAnim = new Animation();
		mrAnim.addFrame(tankImage1, 250000000);
		mrAnim.addFrame(tankImage2, 250000000);
	}
	
	public void loadMoveDownAnim() {
		BufferedImage tankImage1 = loadImage("dtank1.png");
		BufferedImage tankImage2 = loadImage("dtank2.png");
		
		mdAnim = new Animation();
		mdAnim.addFrame(tankImage1, 250000000);
		mdAnim.addFrame(tankImage2, 250000000);
	}
	
	public void loadMoveLeftAnim() {
		BufferedImage tankImage1 = loadImage("ltank1.png");
		BufferedImage tankImage2 = loadImage("ltank2.png");
		
		mlAnim = new Animation();
		mlAnim.addFrame(tankImage1, 250000000);
		mlAnim.addFrame(tankImage2, 250000000);
	}
	
	 public void draw(Graphics2D g) {
	    	g.drawImage(sprite.getImage(), (int)sprite.getx(), (int)sprite.gety(), null);
	 }
	
	public void updateSprite(long time) {
		sprite.setx((float)this.getX());
		sprite.sety((float)this.getY());
		sprite.update(time);
	}
	
	public void startMoving(int dir){
		if (dir == 0 ) {
			moveUp = true;
			moveRight = false;
			moveDown = false;
			moveLeft = false;			
		}
		else if (dir == 1) {
			moveRight = true;
			moveUp = false;
			moveDown = false;
			moveLeft = false;		
		}
		else if (dir == 2) {
			moveDown = true;
			moveUp = false;
			moveRight = false;
			moveLeft = false;
		}
		else if (dir == 3) {
			moveLeft = true;
			moveUp = false;
			moveRight = false;
			moveDown = false;
		}
	}
	
	public void updateDir() { 
		if (moveUp == true) { 
		currentAnim = muAnim; 
		sprite.setAnim(currentAnim); 
		} 

		if (moveRight == true) { 
		currentAnim = mrAnim; 
		sprite.setAnim(currentAnim); 
		} 

		if (moveDown == true) { 
		currentAnim = mdAnim; 
		sprite.setAnim(currentAnim); 
		} 

		if (moveLeft == true) { 
		currentAnim = mlAnim; 
		sprite.setAnim(currentAnim); 
		} 
	}
	
	public void stopMoving(int dir){
		if (dir == 0 ) {
			moveUp = false;
			currentAnim = upAnim;
			sprite.setAnim(currentAnim);
		}
		else if (dir == 1) {
			moveRight = false;
			currentAnim = rightAnim;
			sprite.setAnim(currentAnim);
		}
		else if (dir == 2) {
			moveDown = false;
			currentAnim = downAnim;
			sprite.setAnim(currentAnim);
		}
		else if (dir == 3) {
			moveLeft = false;
			currentAnim = leftAnim;
			sprite.setAnim(currentAnim);
		}
		
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public void setX(double x) {
		this.x = x;
	}
	
	public void setY(double y) {
		this.y = y;
	}
	
	public double getVelocity() {
		return velocity;
	}
	
	public boolean isMoveUp() {
		return moveUp;
	}
	
	public boolean isMoveRight() {
		return moveRight;
	}
	
	public boolean isMoveDown() {
		return moveDown;
	}
	
	public boolean isMoveLeft() {
		return moveLeft;
	}
	
	public BufferedImage loadImage(String fnm) {        
			 try {       
				 BufferedImage im = ImageIO.read(getClass().getResource(fnm));
				 return im;
			     
			 }     
			 catch(IOException e) {
				 System.out.println("Load Image error for " + fnm + ":\n" + e);    
				 return null;
			 }  
		 } // end of loadImage()
	
	public void loadAnims() {
		loadUpAnim();
		loadRightAnim();
		loadDownAnim();
		loadleftAnim();
		loadMoveUpAnim();
		loadMoveRightAnim();
		loadMoveDownAnim();
		loadMoveLeftAnim();
	}
}
