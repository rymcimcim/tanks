import java.awt.Image;

public class Sprite {
    
    private Animation anim;
       
    private float x;
    private float y;
    
    public Sprite(Animation anim, float x, float y){
     
        this.anim = anim;
        this.x = x;
        this.y = y;
    }
    
    public void setAnim(Animation anim){
        this.anim = anim;
    }
    
    public Image getImage(){
       return anim.getImage();
    }
    
    public void update(long time){	
        anim.update(time);
    }
    
    public void setx(float x){
        this.x = x;
    }
    
    public void sety(float y){
        this.y = y;
    }
    
    public int getx(){
        return (int)x;
    }
    
    public int gety(){
        return (int)y;
    }

}